# Python packages
Install all required packages using:

`pip install -r requirements.txt`

# Host locally
`sphinx-autobuild . _build/html`

# Sphinx RTD documentation
https://sphinx-rtd-theme.readthedocs.io/en/stable/index.html

# reStructuredText 
https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
