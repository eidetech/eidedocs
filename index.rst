

eidedocs
====================================

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Academic:

   src/math/math
   src/electrical/electrical

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Engineering Work:

   src/pcb_design/pcb_design
   src/programming/programming

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Concert Industry:

   src/concert_industry/concert_industry

.. toctree::
   :numbered:
   :maxdepth: 1
   :caption: Sphinx examples
   
   src/examples/examples