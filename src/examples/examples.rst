Some Sphinx examples
====================

png figure
#########
.. figure:: ../../fig/png/example.png
   :align: center
   :width: 500

gif figure
##########
.. figure:: ../../fig/gif/example.gif
   :align: center